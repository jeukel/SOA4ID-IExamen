package tech.djenkins.jeukel.iexamen;

public interface IceCream {

    IceCream preparar();
    String obtenerIngredientes();

    class Vainilla implements IceCream {

        private String mainIng = "Vainilla";

         public String obtenerIngredientes(){
            return "Ingrediente Principal="+this.mainIng;
        }

        @Override
        public IceCream preparar() {
            return new Vainilla();
        }
    }

    class Chocomenta implements IceCream {

        private String mainIng = "Chocomenta";

        public String obtenerIngredientes(){
            return "Ingrediente Principal="+this.mainIng;
        }

        @Override
        public IceCream preparar(){
            return new Chocomenta();
        }

    }

    class Sorbetera implements IceCream {

        private String mainIng = "Sorbetera";

        public String obtenerIngredientes(){
            return "Ingrediente Principal="+this.mainIng;
        }

        @Override
        public IceCream preparar(){
            return new Sorbetera();
        }

    }
}