package tech.djenkins.jeukel.iexamen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    String item;

    public enum Sabor {
        VAINILLA,CHOCOMENTA,SORBETERA
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //element
        Spinner spinner = findViewById(R.id.spinner);

        //button action
        final Button button = findViewById(R.id.btnService);
        button.setOnClickListener(new View.OnClickListener() {
            TextView printboard = findViewById(R.id.board);
            public void onClick(View v) {
                IceCream ice = IceCreamFactory.getIceCream(item);
                printboard.setText(ice.obtenerIngredientes());
            }
        });

        //click listener
        spinner.setOnItemSelectedListener(this);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.flavours_array, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // On selecting a spinner item
        //int some = parent.getItemAtPosition(pos);
        item = parent.getItemAtPosition(pos).toString();
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }





}
