package tech.djenkins.jeukel.iexamen;

import android.util.Log;

public class IceCreamFactory {

    public static IceCream getIceCream(String sabor){

        Log.d("SWITCH", "GET ICE CREAM");

        switch (sabor) {
            case "Vainilla":
                {
                    return new IceCream.Vainilla().preparar();
                }

            case "Chocomenta":
                {
                    return new IceCream.Chocomenta().preparar();
                }

            case "Sorbetera":
                {
                    return new IceCream.Sorbetera().preparar();
                }

            default: return null;
        }
    }
}

